#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

typedef unsigned char byte;

struct ppm_header {
    int width;
    int height;
    int max_val;
    int channel_size;
};

struct rgb {
    byte* r;
    byte* g;
    byte* b;
};

struct ycbcr {
    char* y;
    char* cb;
    char* cr;
};

struct dct {
    float* f_y;
    float* f_cb;
    float* f_cr;
};

/***** PPM parsing *****/
int ppm_load_from_file(const char* file_name, struct ppm_header* header, struct rgb* rgb);
int check_magic_number(FILE* fp);
int parse_int(FILE* fp, int* result);
int get_bytes_length(FILE* fp);
int parse_bytes(FILE* fp, struct rgb* rgb, int expected_size);
/***** RGB to YCbCr conversion *****/
int ppm_rgb_to_ycbcr(struct ppm_header* header, struct rgb* rgb, struct ycbcr* ycbcr);
float ycbcr_component(float c_r, float r, float c_g, float g, float c_b, float b, float c);
char clamp(float value, int min, int max);
/***** Shifting *****/
void ppm_shift(struct ppm_header* header, struct ycbcr* ycbcr, int amount);
/***** 2D-DCT *****/
int ppm_2d_dct(struct ppm_header* header, struct ycbcr* ycbcr, struct dct* dct);
/***** Quantisation *****/
void ppm_quantise(struct ppm_header* header, struct dct* dct);
/***** Output to file *****/
int ppm_output_block_to_file(struct ppm_header* header, struct dct* dct, int block_index, const char* file_name);
/***** Utility *****/
int is_whitespace(char c);
int is_digit(char c);

/***** Error handling *****/
#define PPM_FAILURE 1
#define PPM_FAIL() return PPM_FAILURE
#define PPM_FAIL_CLEAN() \
do { \
    PPM_CLEAN(); \
    PPM_FAIL(); \
} while (0)
#define PPM_LOG_ERROR(fmt, ...) \
fprintf(stderr, PPM_ERROR_TITLE ": " fmt "\n", ##__VA_ARGS__)
#define PPM_LOG_ERROR_AND_FAIL(fmt, ...) \
do { \
    PPM_LOG_ERROR(fmt, ##__VA_ARGS__); \
    PPM_FAIL(); \
} while (0)
#define PPM_LOG_ERROR_AND_FAIL_CLEAN(fmt, ...) \
do { \
    PPM_CLEAN(); \
    PPM_LOG_ERROR_AND_FAIL(fmt, ##__VA_ARGS__); \
} while (0)


int main(int argc, char** argv) {
    struct ppm_header header;
    struct rgb rgb;
    struct ycbcr ycbcr;
    struct dct dct;

    clock_t begin = clock();

    if (argc != 4) {
        fprintf(stderr, "Expected 3 command line arguments "
            "(input image path, block index, output file path), "
            "but got %d\n", argc - 1);
        return 1;
    }

    if (ppm_load_from_file(argv[1], &header, &rgb))
        return 1;

    if (ppm_rgb_to_ycbcr(&header, &rgb, &ycbcr))
        return 1;

    ppm_shift(&header, &ycbcr, -128);

    if (ppm_2d_dct(&header, &ycbcr, &dct))
        return 1;

    ppm_quantise(&header, &dct);

    if (ppm_output_block_to_file(&header, &dct, atoi(argv[2]), argv[3]))
        return 1;

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%lf\n", time_spent);

    return 0;
}

/***** PPM parsing *****/
#define PPM_ERROR_TITLE "PPM input error"

#define PPM_CLEAN() \
do { \
    if (fp) fclose(fp); \
} while (0)
int ppm_load_from_file(const char* file_name, struct ppm_header* header, struct rgb* rgb) {
    FILE *fp = fopen(file_name, "rb");
    if (fp == NULL)
        PPM_LOG_ERROR_AND_FAIL("Unable to open %s\n", file_name);

    if (check_magic_number(fp))
        PPM_FAIL_CLEAN();

    if (parse_int(fp, &header->width))
        PPM_FAIL_CLEAN();
    if (header->width < 0 || header->width > 16384)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Invalid width value: %d", header->width);
    if (header->width % 8 != 0)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Unsupported width value: %d", header->width);

    if (parse_int(fp, &header->height))
        PPM_FAIL_CLEAN();
    if (header->height < 0 || header->height > 16384)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Invalid height value: %d", header->height);
    if (header->height % 8 != 0)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Unsupported height value: %d", header->height);

    if (parse_int(fp, &header->max_val))
        PPM_FAIL_CLEAN();
    if (header->max_val < 0 || header->max_val >= 65536)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Invalid maximum color value: %d", header->max_val);
    if (header->max_val != 255)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Unsupported maximum color value: %d", header->max_val);

    header->channel_size = header->width * header->height;
    int expected_size = header->channel_size * 3;

    if (get_bytes_length(fp) != expected_size)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Expected %d bytes of pixel data, but got: %d",
            expected_size, get_bytes_length(fp));

    if (parse_bytes(fp, rgb, expected_size))
        PPM_FAIL_CLEAN();

    PPM_CLEAN();
    return 0;
}
#undef PPM_CLEAN

int check_magic_number(FILE* fp) {
    byte buffer[3] = "";

    fread(buffer, 3, 1, fp);
    if (!is_whitespace(buffer[2]))
        PPM_LOG_ERROR_AND_FAIL("Expected whitespace after magic number, "
            "but got: 0x%x", buffer[0]);

    buffer[2] = '\0';
    if (strncmp((char*) buffer, "P6", 2) != 0)
        PPM_LOG_ERROR_AND_FAIL("Unsupported magic number: %s", buffer);

    return 0;
}

int parse_int(FILE* fp, int* result) {
    int i;
    byte buffer[11] = "";

    for (i = 0; i < 11; i++) {
        fread(&buffer[i], 1, 1, fp);

        if (is_whitespace(buffer[i])) {
            buffer[i] = '\0';
            break;
        }

        if (!is_digit(buffer[i]))
            PPM_LOG_ERROR_AND_FAIL("Invalid non-digit character: 0x%x", buffer[i]);
    }

    if (i == 11) {
        buffer[10] = '\0';
        PPM_LOG_ERROR_AND_FAIL("Value '%s...' to long to parse", buffer);
    }

    *result = atoi((char*) buffer);
    return 0;
}

int get_bytes_length(FILE* fp) {
    int current;
    int size;

    current = ftell(fp);
    fseek(fp, 0L, SEEK_END);
    size = ftell(fp) - current;
    fseek(fp, current, SEEK_SET);

    return size;
}

#define PPM_CLEAN(fmt, ...) \
do { \
    if (rgb->r) free(rgb->r); \
    if (rgb->g) free(rgb->g); \
    if (rgb->b) free(rgb->b); \
} while (0)
int parse_bytes(FILE* fp, struct rgb* rgb, int expected_size) {
    byte buffer[8192];
    int to_read, read_length, total_read = 0, previous_total;
    int remaining_size = expected_size;

    rgb->r = (byte*) malloc(expected_size / 3);
    rgb->g = (byte*) malloc(expected_size / 3);
    rgb->b = (byte*) malloc(expected_size / 3);

    if (!rgb->r || !rgb->g || !rgb->b)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Memory allocation error");

    while (remaining_size > 0) {
        to_read = remaining_size > sizeof(buffer) ?
            sizeof(buffer) : remaining_size;
        read_length = fread(buffer, to_read, 1, fp);
        if (read_length < 0)
            PPM_LOG_ERROR_AND_FAIL_CLEAN("Expected %d bytes, but found: %d",
                expected_size, total_read);

        previous_total = total_read;
        read_length *= to_read;
        total_read += read_length;
        remaining_size -= read_length;

        if (total_read > expected_size)
            PPM_LOG_ERROR_AND_FAIL_CLEAN("Expected %d bytes, but found at least: %d",
                expected_size, total_read);

        for (int i = previous_total; i < previous_total + read_length; i++) {
            byte buffer_value = buffer[i - previous_total];
            switch (i % 3) {
                case 0:
                    (rgb->r)[i / 3] = buffer_value;
                    break;
                case 1:
                    (rgb->g)[i / 3] = buffer_value;
                    break;
                case 2:
                    (rgb->b)[i / 3] = buffer_value;
                    break;
            }
        }
    }

    return 0;
}
#undef PPM_CLEAN

#undef PPM_ERROR_TITLE

/***** RGB to YCbCr conversion *****/
#define PPM_ERROR_TITLE "RGB to YCBCR error: "

#define PPM_CLEAN(fmt, ...) \
do { \
    if (ycbcr->y) free(ycbcr->y); \
    if (ycbcr->cb) free(ycbcr->cb); \
    if (ycbcr->cr) free(ycbcr->cr); \
} while (0)
int ppm_rgb_to_ycbcr(struct ppm_header* header, struct rgb* rgb, struct ycbcr* ycbcr) {
    ycbcr->y = (char*) malloc(header->channel_size * sizeof(char));
    ycbcr->cb = (char*) malloc(header->channel_size * sizeof(char));
    ycbcr->cr = (char*) malloc(header->channel_size * sizeof(char));

    if (!ycbcr->y || !ycbcr->cb || !ycbcr->cr)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Memory allocation error");

    for (int i = 0; i < header->channel_size; i++) {
        byte r_i = rgb->r[i];
        byte g_i = rgb->g[i];
        byte b_i = rgb->b[i];

        ycbcr->y[i] = clamp(ycbcr_component(0.299f, r_i, 0.587f, g_i, 0.114f, b_i, 0.0f), 0, 255);
        ycbcr->cb[i] = clamp(ycbcr_component(-0.1687f, r_i, -0.3313f, g_i, 0.5f, b_i, 128.0f), 0, 255);
        ycbcr->cr[i] = clamp(ycbcr_component(0.5f, r_i, -0.4187f, g_i, -0.0813f, b_i, 128.0f), 0, 255);
    }

    return 0;
}
#undef PPM_CLEAN

float ycbcr_component(float c_r, float r, float c_g, float g, float c_b, float b, float c) {
    return c_r * r + c_g * g + c_b * b + c;
}

char clamp(float value, int min, int max) {
    return round((value < min ? min : (value > max ? max : value)));
}

#undef PPM_ERROR_TITLE

/***** Shifting *****/

void ppm_shift(struct ppm_header* header, struct ycbcr* ycbcr, int amount) {
    for (int i = 0; i < header->channel_size; i++) {
        ycbcr->y[i] += amount;
        ycbcr->cb[i] += amount;
        ycbcr->cr[i] += amount;
    }
}

/***** 2D-DCT *****/
#define PPM_ERROR_TITLE "2D-DCT error: "

#define DCT_C_0 0.70710678118f
#define PI 3.14159265359f

#define PPM_CLEAN(fmt, ...) \
do { \
    if (dct->f_y) free(dct->f_y); \
    if (dct->f_cb) free(dct->f_cb); \
    if (dct->f_cr) free(dct->f_cr); \
} while (0)
int ppm_2d_dct(struct ppm_header* header, struct ycbcr* ycbcr, struct dct* dct) {
    dct->f_y = (float*) malloc(header->channel_size * sizeof(float));
    dct->f_cb = (float*) malloc(header->channel_size * sizeof(float));
    dct->f_cr = (float*) malloc(header->channel_size * sizeof(float));

    if (!dct->f_y || !dct->f_cb || !dct->f_cr)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Memory allocation error");

    for (int block_i = 0; block_i < header->height / 8; block_i++) {
        for (int block_j = 0; block_j < header->width / 8; block_j++) {
            for (int u = 0; u < 8; u++) {
                for (int v = 0; v < 8; v++) {
                    float c_u = u == 0 ? DCT_C_0 : 1.0f;
                    float c_v = v == 0 ? DCT_C_0 : 1.0f;

                    int dct_index = (block_i * 8 + u) * header->width + (block_j * 8 + v);

                    for (int i = 0; i < 8; i++) {
                        for (int j = 0; j < 8; j++) {
                            float cos_i = cosf(((2 * i + 1) * u * PI)/16);
                            float cos_j = cosf(((2 * j + 1) * v * PI)/16);

                            int ycbcr_index = (block_i * 8 + i) * header->width + (block_j * 8 + j);

                            dct->f_y[dct_index] += ycbcr->y[ycbcr_index] * cos_i * cos_j;
                            dct->f_cb[dct_index] += ycbcr->cb[ycbcr_index] * cos_i * cos_j;
                            dct->f_cr[dct_index] += ycbcr->cr[ycbcr_index] * cos_i * cos_j;
                        }
                    }

                    dct->f_y[dct_index] *= 0.25f * c_u * c_v;
                    dct->f_cb[dct_index] *= 0.25f * c_u * c_v;
                    dct->f_cr[dct_index] *= 0.25f * c_u * c_v;
                }
            }
        }
    }

    return 0;
}
#undef PPM_CLEAN

#undef PPM_ERROR_TITLE

/***** Quantisation *****/

void ppm_quantise(struct ppm_header* header, struct dct* dct) {
    int k_1[] = {
        16, 11, 10, 16, 24, 40, 51, 61,
        12, 12, 14, 19, 26, 58, 60, 55,
        14, 13, 16, 24, 40, 57, 69, 56,
        14, 17, 22, 29, 51, 87, 80, 62,
        18, 22, 37, 56, 68, 109, 103, 77,
        24, 35, 55, 64, 81, 104, 113, 92,
        49, 64, 78, 87, 103, 121, 120, 101,
        72, 92, 95, 98, 112, 100, 103, 99
    };

    int k_2[] = {
        17, 18, 24, 47, 99, 99, 99, 99,
        18, 21, 26, 66, 99, 99, 99, 99,
        24, 26, 56, 99, 99, 99, 99, 99,
        47, 66, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99
    };

    for (int block_i = 0; block_i < header->height / 8; block_i++) {
        for (int block_j = 0; block_j < header->width / 8; block_j++) {
            for (int u = 0; u < 8; u++) {
                for (int v = 0; v < 8; v++) {
                    int dct_index = (block_i * 8 + u) * header->width + (block_j * 8 + v);
                    int table_index = u * 8 + v;

                    dct->f_y[dct_index] /= k_1[table_index];
                    dct->f_cb[dct_index] /= k_2[table_index];
                    dct->f_cr[dct_index] /= k_2[table_index];

                    dct->f_y[dct_index] = roundf(dct->f_y[dct_index]);
                    dct->f_cb[dct_index] = roundf(dct->f_cb[dct_index]);
                    dct->f_cr[dct_index] = roundf(dct->f_cr[dct_index]);
                }
            }
        }
    }
}

/***** Output to file *****/
#define PPM_ERROR_TITLE "PPM output error: "

#define PPM_CLEAN() \
do { \
    if (fp) fclose(fp); \
} while (0)
int ppm_output_block_to_file(struct ppm_header* header, struct dct* dct, int block_index, const char* file_name) {
    FILE *fp = fopen(file_name, "w");
    if (fp == NULL)
        PPM_LOG_ERROR_AND_FAIL("Unable to create/overwrite %s", file_name);

    if (block_index < 0 || block_index >= header->channel_size / 64)
        PPM_LOG_ERROR_AND_FAIL_CLEAN("Invalid block index: %d", block_index);

    int block_i = block_index / (header->width / 8);
    int block_j = block_index % (header->width / 8);

    float* channels[] = { dct->f_y, dct->f_cb, dct->f_cr };

    for (int c = 0; c < 3; c++) {
        float* channel = channels[c];
        for (int u = 0; u < 8; u++) {
            for (int v = 0; v < 8; v++) {
                int index = (block_i * 8 + u) * header->width + (block_j * 8 + v);

                fprintf(fp, "%d%s", (int) channel[index], v == 7 ? "" : " ");
            }

            fprintf(fp, "\n");
        }

        if (c < 2)
            fprintf(fp, "\n");
    }

    return 0;
}
#undef PPM_CLEAN

#undef PPM_ERROR_TITLE

/***** Utility *****/

int is_whitespace(char c) {
    return isspace(c);
}

int is_digit(char c) {
    return c >= '0' && c <= '9';
}
