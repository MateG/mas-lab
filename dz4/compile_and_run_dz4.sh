#!/bin/bash

INCLUDE_PATH=/home/mate/intel/oneapi/ipp/latest/include
LINK_PATH=/home/mate/intel/oneapi/ipp/latest/lib/intel64

gcc dz4.c -lm -I$INCLUDE_PATH -L$LINK_PATH -lippi -lippcore
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LINK_PATH ./a.out "$@"
