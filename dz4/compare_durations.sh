#!/bin/bash

TEST_COUNT=10
DEFAULT_ARGS="lenna.ppm 0 out.txt"

args="$@"
if [ "$#" -ne 4 ]
then
    echo "Expected 3 args, but got $#... Using default args"
    args="$DEFAULT_ARGS"
fi
echo "Running $TEST_COUNT times using args \"$args\""

sum=0
for i in $(seq 1 $TEST_COUNT)
do
    output=$(./compile_and_run_dz3.sh $args)
    sum=$(bc <<< "$sum+$output")
done
average=$(bc <<< "scale=4;$sum/$TEST_COUNT")
printf "DZ3 %.4f\n" $average

sum=0
for i in $(seq 1 $TEST_COUNT)
do
    output=$(./compile_and_run_dz4.sh $args)
    sum=$(bc <<< "$sum+$output")
done
average=$(bc <<< "scale=4;$sum/$TEST_COUNT")
printf "DZ4 %.4f\n" $average
