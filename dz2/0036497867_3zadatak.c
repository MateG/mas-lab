#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef unsigned char byte;

/***** PGM parsing and data processing *****/
void check_magic_number(FILE* fp);
void parse_int(FILE* fp, int* result);
void process_probabilities(FILE* fp, int width, int height);
/***** Error-checked file operations *****/
FILE* checked_fopen(const char* file_path, const char* mode);
int checked_fread(void* buffer, size_t size, size_t quantity, FILE* fp);
/***** Utility *****/
int is_whitespace(char c);
int is_digit(char c);

int main(int argc, char** argv) {
    FILE* fp;
    int width;
    int height;
    int max_val;

    if (argc != 2) {
        fprintf(stderr, "Expected 1 command line argument (input image path), "
            "but got %d\n", argc - 1);
        return EXIT_FAILURE;
    }

    fp = checked_fopen(argv[1], "rb");
    check_magic_number(fp);
    parse_int(fp, &width);
    parse_int(fp, &height);
    parse_int(fp, &max_val);
    if (max_val != 255) {
        fprintf(stderr, "Unsupported maximum value: %d\n", max_val);
        fclose(fp);
        return EXIT_FAILURE;
    }

    process_probabilities(fp, width, height);

    fclose(fp);
    return EXIT_SUCCESS;
}

/***** PGM parsing and data processing *****/

void check_magic_number(FILE* fp) {
    byte buffer[3] = "";
    int bytes_read;

    bytes_read = fread(buffer, 1, 3, fp);
    if (bytes_read != 3) {
        fprintf(stderr, "Unexpected end of file while checking the magic number\n");
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    if (!is_whitespace(buffer[2])) {
        fprintf(stderr, "Expected whitespace after magic number, but got: 0x%x\n", buffer[0]);
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    buffer[2] = '\0';
    if (strncmp((char*) buffer, "P5", 2) != 0) {
        fprintf(stderr, "Unsupported magic number: %s\n", buffer);
        fclose(fp);
        exit(EXIT_FAILURE);
    }
}

void parse_int(FILE* fp, int* result) {
    int i;
    byte buffer[11] = "";

    for (i = 0; i < 11; i++) {
        fread(&buffer[i], 1, 1, fp);

        if (is_whitespace(buffer[i])) {
            buffer[i] = '\0';
            break;
        }

        if (!is_digit(buffer[i])) {
            fprintf(stderr, "Invalid non-digit character: 0x%x\n", buffer[i]);
            fclose(fp);
            exit(EXIT_FAILURE);
        }
    }

    if (i == 11) {
        buffer[10] = '\0';
        fprintf(stderr, "Value '%s...' to long to parse\n", buffer);
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    *result = atoi((char*) buffer);
}

void process_probabilities(FILE* fp, int width, int height) {
    int group_counters[16] = {0};
    byte read_byte;

    for (int i = 0; i < width * height; i++) {
        checked_fread(&read_byte, 1, 1, fp);
        group_counters[read_byte >> 4] += 1;
    }

    for (int i = 0; i < 16; i++) {
        printf("%d %f\n", i, (float) group_counters[i] / (width * height));
    }
}

/***** Error-checked file operations *****/

FILE* checked_fopen(const char* file_path, const char* mode) {
    FILE* fp;

    fp = fopen(file_path, mode);
    if (fp == NULL) {
        fprintf(stderr, "Error while opening %s\n", file_path);
        exit(EXIT_FAILURE);
    }

    return fp;
}

int checked_fread(void* buffer, size_t size, size_t quantity, FILE* fp) {
    int bytes_read;

    bytes_read = fread(buffer, size, quantity, fp);
    if (feof(fp)) {
        fprintf(stderr, "Error while reading from file\n");
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    return bytes_read;
}

/***** Utility *****/

int is_whitespace(char c) {
    return isspace(c);
}

int is_digit(char c) {
    return c >= '0' && c <= '9';
}
