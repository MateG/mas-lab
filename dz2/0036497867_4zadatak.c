#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

typedef unsigned char byte;

struct position {
    int row;
    int col;
};

/***** PGM parsing and data processing *****/
void check_header_values(FILE* fp);
void check_block_index(int block_index, int min, int max);
double calculate_mad(byte* dst_bytes, struct position dst_start, byte* src_bytes, struct position src_start);
/***** Error-checked file operations *****/
FILE* checked_fopen(const char* file_path, const char* mode);
int checked_fread(void* buffer, size_t size, size_t quantity, FILE* fp);

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Expected 1 command line argument (block index), "
            "but got %d\n", argc - 1);
        return EXIT_FAILURE;
    }

    int dst_block_index = atoi(argv[1]);
    check_block_index(dst_block_index, 0, 1023);
    struct position dst_block = { dst_block_index / 32, dst_block_index % 32 };

    FILE* dst_fp = checked_fopen("lenna1.pgm", "rb");
    check_header_values(dst_fp);

    byte dst_bytes[512 * 512];
    checked_fread(dst_bytes, 1, 512 * 512, dst_fp);
    fclose(dst_fp);

    FILE* src_fp = checked_fopen("lenna.pgm", "rb");
    check_header_values(src_fp);

    byte src_bytes[512 * 512];
    checked_fread(src_bytes, 1, 512 * 512, src_fp);
    fclose(src_fp);

    double lowest_mad = DBL_MAX;
    struct position lowest_mad_src;

    struct position dst_start = { dst_block.row * 16, dst_block.col * 16 };
    for (int start_row = dst_start.row - 16; start_row <= dst_start.row + 16; start_row++) {
        if (start_row < 0) continue;
        if (start_row >= 512 - 15) break;
        for (int start_col = dst_start.col - 16; start_col <= dst_start.col + 16; start_col++) {
            if (start_col < 0) continue;
            if (start_col >= 512 - 15) break;
            struct position src_start = { start_row, start_col };
            double mad = calculate_mad(dst_bytes, dst_start, src_bytes, src_start);
            if (mad < lowest_mad) {
                lowest_mad = mad;
                lowest_mad_src = (struct position) { start_row, start_col };
            }
        }
    }
    printf("%d,%d\n", lowest_mad_src.col - dst_start.col, lowest_mad_src.row - dst_start.row);

    return EXIT_SUCCESS;
}

/***** PGM parsing and data processing *****/

void check_header_values(FILE* fp) {
    char buffer[15];

    checked_fread(buffer, 1, 15, fp);
    if (strncmp(buffer, "P5\n512\n512\n255\n", 15) != 0) {
        fprintf(stderr, "Unexpected header values\n");
        fclose(fp);
        exit(EXIT_FAILURE);
    }
}

void check_block_index(int block_index, int min, int max) {
    if (block_index < min || block_index > max) {
        fprintf(stderr, "Block index must be in [%d-%d] range\n", min, max);
        exit(EXIT_FAILURE);
    }
}

double calculate_mad(byte* dst_bytes, struct position dst_start, byte* src_bytes, struct position src_start) {
    double sum = 0.0;
    for (int row = 0; row < 16; row++) {
        for (int col = 0; col < 16; col++) {
            int r = dst_start.row + row;
            int c = dst_start.col + col;
            if (r * 512 + c >= 512*512) {
                printf("!!!\n");
            }
            byte dst_byte = dst_bytes[r * 512 + c];
            r = src_start.row + row;
            c = src_start.col + col;
            if (r * 512 + c >= 512*512) {
                printf("!!!!\n");
            }
            byte src_byte = src_bytes[r * 512 + c];
            double difference = src_byte - dst_byte;
            sum += abs(difference);
        }
    }
    return sum / (16 * 16);
}

/***** Error-checked file operations *****/

FILE* checked_fopen(const char* file_path, const char* mode) {
    FILE* fp;

    fp = fopen(file_path, mode);
    if (fp == NULL) {
        fprintf(stderr, "Error while opening %s\n", file_path);
        exit(EXIT_FAILURE);
    }

    return fp;
}

int checked_fread(void* buffer, size_t size, size_t quantity, FILE* fp) {
    int bytes_read;

    bytes_read = fread(buffer, size, quantity, fp);
    if (feof(fp)) {
        fprintf(stderr, "Error while reading from file\n");
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    return bytes_read;
}
